import * as types from "../types";

const initialState = {
  fetchedData: [],
  currentShowingData: {}
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.SET_DATA:
      return {
        ...state,
        fetchedData: action.payload
      };

    case types.SET_CLASS_DATA:
      return {
        ...state,
        currentShowingData: state.fetchedData[action.index]
      };

    default:
      return state;
  }
};
