import React from "react";
import Layout from "./components/Layout/Layout";
import { Provider } from "react-redux";
import store from "./store";

export default function App() {
  return (
    <Provider store={store}>
      <Layout />
    </Provider>
  );
}
