import * as types from "../types";

export const fetchData = () => {
  return {
    type: types.FETCH_DATA
  };
};

export const setClassData = index => {
  return {
    type: types.SET_CLASS_DATA,
    index
  };
};
