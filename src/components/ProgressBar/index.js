import React from "react";
import styled from "styled-components";

const ContainerBar = styled.div`
  margin-left:0;
  margin-top:8px;
  margin-right:10px; 
  border-radius: 5px;
  background-color: #e3e3e3;
  width: 100px;
  height : 8px
`;

const Bar = styled.div`
  border-radius: 5px;
  width: ${props => (props.width ? props.width + "px" : "10px")};
  background-color: #4d4dae;
  position: relative;
  top: 0;
  left: 0;
  height: 8px;
`;

const Flex = styled.div`
  display: flex;
  justify-content: space-between;
`;

const P = styled.p`
  width : 43px;
  color: #5ebbf3;
  font-size: 1.1rem;
`;

export default function ProgressBar({ marks }) {
  return (
    <Flex>
      <ContainerBar>
        <Bar width={marks} />
      </ContainerBar>
      <P>{marks}%</P>
    </Flex>
  );
}
