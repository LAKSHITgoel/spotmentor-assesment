import React from "react";
import styled from "styled-components";

const Li = styled.li`
  cursor: pointer;
  list-style: none;
  font-size: 1.1em;
  color: #fff;
  padding: 15px;
  text-align: center;
  border-left: ${props => (props.active ? "5px solid #fff" : "none")};
  background-color: ${props => (props.active ? "#4d4dae" : "none")};
`;

export default function ClassItem(props) {
  return <Li {...props}>{props.value}</Li>;
}
