import React from "react";
import styled from "styled-components";
import ProgressBar from "../ProgressBar";

const StyledCard = styled.div`
  min-width: 300px;
  color: #000;
  background-color: #fff;
  border-radius: 5px;
  padding: 14px;
`;

const CardHeader = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 10px;
  white-space: nowrap;
`;

const Result = styled.div`
  padding: 10px;
  display: flex;
  justify-content: space-between;
`;

const P = styled.p`
  width: 43px;
  color: ${props => (props.primary ? "#4d4dae" : "#5ebbf3")};
  font-size: 1rem;
`;

const getAverage = marks => {
  let arr = Object.values(marks);
  let sum = arr.reduce((a, c) => a + c);
  return Math.floor(sum / arr.length);
};

export default function Card({ name, marks }) {
  return (
    <StyledCard>
      <CardHeader>
        <P primary>{name[0].toUpperCase() + name.slice(1)}</P>
        <P>{getAverage(marks)}%</P>
      </CardHeader>
      <div>
        {Object.keys(marks).map(subject => (
          <Result key={subject}>
            <p>{subject}</p>
            <ProgressBar marks={marks[subject]} />
          </Result>
        ))}
      </div>
    </StyledCard>
  );
}
