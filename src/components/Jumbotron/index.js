import React from "react";
import styled from "styled-components";

const StyledDiv = styled.div`
  padding: 40px;
  margin : 40px;
  background-color: #fff;
  border-radius: 5px;
  text-align: center;
  font-size: 1rem;
`;

export default function Jumbotron(props) {
  return <StyledDiv>{props.children}</StyledDiv>;
}
