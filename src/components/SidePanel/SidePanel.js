import React from "react";
import styled from "styled-components";
import ClassItem from "../ClassItem";
import { connect } from "react-redux";
import { setClassData } from "../../action/homeAction";
import { bindActionCreators } from "redux";

const StyledSidePanel = styled.div`
  flex: 2;
  background: linear-gradient(180deg, #4e4eae, #8282c2);
  height: 100vh;
`;

const StyledHeading = styled.h2`
  color: #fff;
  font-weight: 100;
  text-align: center;
  padding: 20px;
`;

function SidePanel(props) {
  return (
    <StyledSidePanel>
      <StyledHeading>School XYZ</StyledHeading>
      <ul>
        {props.data.map((obj, index) => (
          <ClassItem
            value={obj.classname}
            key={`_${index}`}
            onClick={() => props.setClassData(index)}
            active={props.current.classname === obj.classname}
          />
        ))}
      </ul>
    </StyledSidePanel>
  );
}

const mapStateToProps = state => ({
  data: state.home.fetchedData,
  current: state.home.currentShowingData
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ setClassData }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SidePanel);
