import React from "react";
import styled from "styled-components";

const StyledButton = styled.button`
    padding: 10px 15px 10px 15px;
    border-radius: 20px;
    border : none;
    color : ${props => (props.active ? "#fff" : "#000")}
    background-color : ${props => (props.active ? "#4d4dae" : "#fff")}
    font-size : 1rem;
`;

export default function Button(props) {
  return <StyledButton {...props} />;
}
