import React, { useState } from "react";
import Button from "../Button";
import styled from "styled-components";
import Card from "../Card";
import Jumbotron from "../Jumbotron";

const StyledMainPanel = styled.div`
  flex: 8;
  background-color: #d4d4d4;
  height: 100vh;
`;

const Flex = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: ${props => (props.wrap ? "wrap" : "nowrap")};
  padding: 25px;
`;

export default function MainPanel(props) {
  const [active, setActive] = useState(false);

  const getAverage = marks => {
    let arr = Object.values(marks);
    let sum = arr.reduce((a, c) => a + c);
    return Math.floor(sum / arr.length);
  };

  const getClassroomAverage = () => {
    let arr = props.data.students.map(obj => getAverage(obj.marks));
    let sum = arr.reduce((a, c) => a + c);
    return Math.floor(sum / arr.length);
  };

  return (
    <StyledMainPanel>
      {Object.keys(props.data).length > 0 ? (
        <>
          <Flex>
            <div>
              <h2>{props.data.classname}</h2>
              <p>{props.data.students.length} Students</p>
            </div>
            <Button active={active} onClick={() => setActive(!active)}>
              {active ? "Hide Average" : "Show Average"}
            </Button>
          </Flex>
          {active && (
            <Jumbotron>
              Classroom Average Performance : {getClassroomAverage()}%
            </Jumbotron>
          )}
          <Flex wrap>
            {props.data.students.map((obj, index) => (
              <Card {...obj} key={`_${index}`} />
            ))}
          </Flex>
        </>
      ) : (
        <h1>Select an Item</h1>
      )}
    </StyledMainPanel>
  );
}
