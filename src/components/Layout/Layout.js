import React, { useState, useEffect } from "react";
import styled from "styled-components";
import MainPanel from "../MainPanel/MainPanel";
import SidePanel from "../SidePanel/SidePanel";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchData } from "../../action/homeAction";

const Flex = styled.div`
  display: flex;
`;

const Layout = props => {
  const [fetched, setFetched] = useState(false);

  useEffect(() => {
    setFetched(true);
    props.fetchData();
  }, [fetched]);

  return (
    <Flex>
      <SidePanel />
      <MainPanel data={props.home.currentShowingData} />
    </Flex>
  );
};

const mapStateToProps = state => ({
  home: state.home
});
const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      fetchData
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Layout);
