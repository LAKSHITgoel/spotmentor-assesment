import * as api from "./api";
import { put, call } from "redux-saga/effects";
import * as types from "../types";


export function* fetchData() {
  let data = yield call(api.fetchData);
  console.log(data);
  yield put({ type: types.SET_DATA, payload: data });
}
