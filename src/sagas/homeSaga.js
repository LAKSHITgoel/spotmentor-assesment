import * as types from "../types";
import { takeLatest } from "redux-saga/effects";
import * as call from "./call";

export function* homeSaga() {
  yield takeLatest(types.FETCH_DATA, call.fetchData);
}